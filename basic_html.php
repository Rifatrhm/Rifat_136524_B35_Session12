<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Page Title</title>
    </head>

    <body>

    <h1>Heading</h1>
        <h1>This is heading</h1>
        <h2>This is heading</h2>
        <h3>This is heading</h3>
        <h4>This is heading</h4>
        <h5>This is heading</h5>
        <h6>This is heading</h6>
        </br>

    <h1>paragraph tag</h1>

    <p>this is large but <small> this text is small</p>
        </br>

    <h1>highlight</h1>

        you can use the mark tag to <mark>highlight</mark>text
        </br>

    <h1>delete</h1>

        <del>this line of text is meant to be treated as deleted text</del>

        </br>

    <h1>underline</h1>

        <u>this text is underlined</u>

        </br>

    <h1>bold</h1>

    <b>this text is bold </b>

        </br>

    <h1>italic</h1>

        <i>this text is italic </i>

        </br>

    <h1>blockquote</h1>

    <blockquote> this is a long quotation. this is along quotation. this is a long quotation. this is a long quotation</blockquote>

        </br>

    <h1>abbr</h1>

        <abbr title="world Health Organization "> WHO </abbr> was founded in 1948.

        </br>

    <h1>acronym</h1>

        Do the work <acronym title="As Soon as Possible">ASAP</acronym>

        </br>

    <h1>address</h1>

        <address>
        written by pondit.com <br>
        <a href="mailto:team@pondit.com">Email us </a><br>

        address: House #16, Road#12,Dhaka <br>

        phone: 01846829005

        </br>

    <h1>BDO</h1>


    <bdo dir= "rtl">
         This is liveoutsource.com
    </bdo>

        </br>

    <h1>big</h1>

    <big>this test is big </big> but this is not.

        </br>

    <h1>cite</h1>

    <cite> this is a citation </cite>

        </br>

    <h1>Code</h1>

    <p>Regular text. <code> This is code.</code>regular text</p>

        </br>

    <h1>DFN</h1>

    <dfn>Definition term </dfn>

    <p> <dfn id=def-internet">The internet</dfn> is a global system of interconnected networks that use the internetconnected
        networks that use the internet protocol  Suite (TCP/IP) to serve billions of Users worldwide </p>

        </br>

    <h1>inserted text </h1>

    <p> I am <del>very</del><ins>extremly</ins> happy that you visited this page </p>

    <h1>Pre tag</h1>

    <pre>this in a pre element,font and it preserves</pre>

        </br>

    <h1>SUB TAG</h1>

    <p>Chemical structure of water is H <sub>2</sub>O</p>

        </br>

    <h1>Sup tag</h1>

    <p>3<sup>3</sup>=27</p>

        </br>

    </body>